﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json.Serialization;

namespace Serialization
{
    class Program
    {
        static void Main(string[] args)
        {
            var converter = new Converter();
            var serialized = converter.GetInput();
            var input = converter.GetInputObject(serialized);
            var output = converter.GetOutputObject(input);

            Console.WriteLine(converter.GetSerializedOutput(output));

//            solver.SetType("Json");
//            var serializedString = "{\"K\":10, \"Sums\":[1.01, 2.02], \"Muls\":[1, 4]}";
//            var input = solver.GetInputObject(serializedString);
//            Console.WriteLine(input);
//            var output = solver.GetOutputObject(input);
//            Console.WriteLine(output);
//            Console.WriteLine(solver.GetSerializedOutput(output));
        }
    }
}


